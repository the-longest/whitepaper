# The longest - Whitepaper

Collect and merge lines to have 'The longest' of all.

## Mechanics

- in the beginning there exist N lines of length 0 as non fungible tokens (NFTs)
- you can either buy it from the owner or mint them with a specific mechanic -> TBD
- you can transfer/sell/buy lines and if you have more that 1 line in your wallet it get merged
  - the old lines gets destroyed and a new line (NFT) gets minted with the new length
- there is the leader board of who have 'The longest' line of all
- after a specific period there could be a drop for the top X wallets in the leader board
- the one who have currently the longest can deposit a portion of the royalties

##

- mint for the big players in the dot game

## Similar projects

- the dot collection project (ask Gabriel about the name)
